AM_CFLAGS =  -fstack-protector -Wall -pedantic \
        -Wstrict-prototypes -Wundef -fno-common \
        -Wformat -Wformat-security \
        -Wno-conversion \
        -DDATA_DIR=\"$(datadir)\"
